This numerically calculates x(t) and y(t) for a particle in a constant gravitational field on a line given by the function y(x). It will plot y(x), v(t), and animate y(x) time dependently as matplotlib animation or gif.

Here, most important the brachistochrone (minimizing the run time going from one point to another) is compared to other lines.

Simply run by

```
python brach_numerik.py
```