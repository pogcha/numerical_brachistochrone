#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 10:56:17 2020

@author: malte
"""
import numpy as np
from scipy.optimize import brentq
import matplotlib.pyplot as plt
from scipy.special import *
from scipy.interpolate import interp1d
import matplotlib.animation as animation

make_movie = False
animate_slide = False
save_plots = False

g = 10.0
y0 = 1.0
x0 = np.pi/2.0

ratio = x0/y0
m = -y0/x0

# function to find theta0. This has to be zero to satisfy theta0 for a
# certain ratio of y0 and x0
def f(theta0):
    return (theta0 - np.sin(theta0))/(1.0-np.cos(theta0)) - ratio

def y_func(theta):
    return y0-r*(1-np.cos(theta))

def x_func(theta):
    return r*(theta-np.sin(theta))
    
theta0 = brentq(f,-100,1000)
theta = np.linspace(0,theta0+0.1,1000)
c = np.sqrt(4*g*x0/(theta0-np.sin(theta0)))

r = c**2/4.0/g

# here we interpolate x(theta) and y(theta)
x = x_func(theta)
y = y_func(theta)

# we calculate numerically dy/dx and interpolate to get a function y'(x)
f_grad = interp1d(x, np.gradient(y)/np.gradient(x),kind='cubic')

# these are exponential slides
x_exp = np.linspace(0,x0,1000)
lambda_exp_1 = 10.0
a_exp_1 = y0/(1.0-np.exp(-lambda_exp_1*x0))
b_exp_1 = a_exp_1*np.exp(-lambda_exp_1*x0)
y_exp_1 = a_exp_1*np.exp(-lambda_exp_1*x_exp)-b_exp_1

lambda_exp_2 = 1.0
a_exp_2 = y0/(1.0-np.exp(-lambda_exp_2*x0))
b_exp_2 = a_exp_2*np.exp(-lambda_exp_2*x0)
y_exp_2 = a_exp_2*np.exp(-lambda_exp_2*x_exp)-b_exp_2


# numerical parameters for the integration
dt = 0.0001
max_steps = 20000

# this integrates for a given funtion y'(x)
def integrate(dydx_func):
    x = [0]
    y = [y0]
    v = [0]
    t = [0]
    while t[-1]<dt*max_steps and x[-1]<x0:
        dydx = dydx_func(x[-1])
        # integrate acceceration
        a = - g * dydx/np.sqrt(dydx**2+1.0)
        v.append(v[-1]+a*dt)
        
        # use energy conservation to get dy
        dy = +y0 - y[-1] - 0.5/g*(v[-1])**2
        if np.abs(dydx) > 1e-8:
            dx = dy/dydx
        else:
            
            # if dydx is very small, then dy will be very small
            # then dy/dydx will give numerical problems
            # we then use the pythagorean rule to get dx from
            # the current speed along the line and that in 
            # y-direction.
            # this only really occurs for the exponential function with large 
            # lambda
            
            dx = dt*np.sqrt((v[-1])**2 - (dy/dt)**2)
            
        x.append(x[-1] + dx)
        y.append(y[-1] + dy)
        t.append(t[-1]+dt)
    return x, y, v, np.array(t)


# these are analytical expresions for y'(x)
def dydx_lin(x):
    return m

def dydx_exp_1(x):
    return -a_exp_1*lambda_exp_1*np.exp(-lambda_exp_1*x)

def dydx_exp_2(x):
    return -a_exp_2*lambda_exp_2*np.exp(-lambda_exp_2*x)


# we integrate all 4 cases
x_lin, y_lin, v_lin, t_lin = integrate(dydx_lin)

x_exp_1, y_exp_1, v_exp_1, t_exp_1 = integrate(dydx_exp_1)

x_exp_2, y_exp_2, v_exp_2, t_exp_2 = integrate(dydx_exp_2)

x_brach, y_brach, v_brach, t_brach = integrate(f_grad)

# here we plot the results
plt.figure(1,figsize=(5,3.5))
plt.plot(x[x<x0],y[x<x0],'-C0')
plt.plot(x_exp_1, y_exp_1,'-C2')
plt.plot(x_exp_2, y_exp_2,'-C3')
plt.plot([0,x0],[y0,0],'-C1')
plt.plot(0,y0,'ok',zorder=10)
plt.plot(x0,0,'ok',zorder=10)
plt.plot(x_brach[::300],y_brach[::300],'oC0')
plt.plot(x_lin[::300],y_lin[::300],'oC1')
plt.plot(x_exp_1[::300],y_exp_1[::300],'oC2')
plt.plot(x_exp_2[::300],y_exp_2[::300],'oC3')
plt.xlabel('x')
plt.ylabel('y')
plt.tight_layout()
if save_plots:
    plt.savefig('x_y_x0_{0:1.1f}.pdf'.format(x0),fmt='pdf')
    
plt.figure(2,figsize=(5,3.5))
plt.plot(t_lin, v_lin,'C1',label='linear slide')
plt.plot(t_brach, v_brach,'C0',label='Brachistochrone')
plt.plot(t_exp_1, v_exp_1,'C2',label='exp1')
plt.plot(t_exp_2, v_exp_2,'C3',label='exp2')
plt.plot(t_brach[t_brach<np.sqrt(2/g*y0)],g*t_brach[t_brach<np.sqrt(2/g*y0)],'C4',label='free fall')
plt.legend(loc='lower right')
plt.xlabel('t')
plt.ylabel('v')
plt.tight_layout()
if save_plots:
    plt.savefig('v_t_x0_{0:1.1f}.pdf'.format(x0),fmt='pdf')

# here we print the time needed for sliding
print('time for linear slide',t_lin[-1])
print('time for Brachistochrone slide',t_brach[-1])
print('time for exp. slide 1',t_exp_1[-1])
print('time for exp. slide 2',t_exp_2[-1])
print('time for free fall',np.sqrt(2/g*y0))
print('analytical expression for x0=pi/2*y0',np.sqrt(2/g*y0)*np.pi/2.0)

# if wanted, the sliding is animated
if animate_slide or make_movie:
    fig = plt.figure(7,figsize=(5,3.5))
    min_scale =np.min([0.0,np.min(y_brach)])
    ax = fig.add_subplot(111,  autoscale_on=False,
                     xlim=(-0.1, 1.1*x0), ylim=(min_scale-0.1, y0*1.1))
    ax.plot(x[x<x0],y[x<x0],'-C0')
    ax.plot([0,x0],[y0,0],'-C1')
    ax.plot(x_exp_1,y_exp_1,'-C2')
    ax.plot(x_exp_2,y_exp_2,'-C3')
    ax.plot()
    ax.plot(0,y0,'ok',zorder=10)
    ax.plot(x0,0,'ok',zorder=10)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.xticks([])
    plt.yticks([])
    plt.tight_layout()

    dot_brach, = ax.plot([0], [y0], 'oC0')
    dot_lin, = ax.plot([0], [y0], 'oC1')
    dot_exp_1, = ax.plot([0], [y0], 'oC2')
    dot_exp_2, = ax.plot([0], [y0], 'oC3')
    time_text = ax.text(0.9, 0.9, '', transform=ax.transAxes,horizontalalignment='right')
    #ax.grid()
    interval = 30
    step_factor = 150

def init():
    """initialize animation"""
    dot_brach.set_data([], [])
    dot_lin.set_data([], [])
    dot_exp_1.set_data([], [])
    dot_exp_2.set_data([], [])
    time_text.set_text('')
    #energy_text.set_text('')
    return dot_brach, dot_lin, dot_exp_1, dot_exp_2, time_text

def animate(i):
    """perform animation step"""
    #pendulum.step(dt)
    try:
        dot_brach.set_data([x_brach[step_factor*i]],[y_brach[step_factor*i]])
    except:
        dot_brach.set_data([x_brach[-1]],[y_brach[-1]])
    try:
        dot_exp_1.set_data([x_exp_1[step_factor*i]],[y_exp_1[step_factor*i]])
    except:
        dot_exp_1.set_data([x_exp_1[-1]],[y_exp_1[-1]])
    try:
        dot_exp_2.set_data([x_exp_2[step_factor*i]],[y_exp_2[step_factor*i]])
    except:
        dot_exp_2.set_data([x_exp_2[-1]],[y_exp_2[-1]])
    dot_lin.set_data([x_lin[step_factor*i]],[y_lin[step_factor*i]])
    time_text.set_text('time = %.2f' % t_lin[step_factor*i])
    return dot_brach, dot_lin,dot_exp_1,dot_exp_2, time_text
    #time_text.set_text('time = %.1f' % pendulum.time_elapsed)
    #energy_text.set_text('energy = %.3f J' % pendulum.energy())
    #return line, time_text, energy_text


if animate_slide:

    ani = animation.FuncAnimation(fig, animate, frames=int(len(t_lin)/step_factor),
                              interval=interval, blit=True, init_func=init)
    plt.show()
    

# if wanted, the animation is saved as a gif
if make_movie:
    # output the animation to gif
    moviewriter = animation.ImageMagickWriter(fps=30)
    moviewriter.setup(fig=fig, outfile='x_y_x0_{0:1.1f}.gif'.format(x0), dpi=100)
    for ii in range(int(len(t_lin)/step_factor)):
        animate(ii)
        moviewriter.grab_frame()
    moviewriter.finish()
